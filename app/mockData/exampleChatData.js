const initialMessages = [
  {from: '0x0...', text: 'How can i learn cool Stuff about magical healing stones?', type: 'message'},
  {from: 'SERVER', text: 'OHO!', type: 'alert'},
  {from: 'SERVER', text: '0x0 joined', type: 'notification'}  
];

const mockChats1 = [
{ 'id': 101, 'user_id': 12345, 'timestamp': 1509331303568, 'message': 'Howdy do?'},
{ 'id': 102, 'user_id': 23456, 'timestamp': 1509331353568, 'message': 'Howdy dont?'},
{ 'id': 103, 'user_id': 34567, 'timestamp': 1509332403568, 'message': 'Howdy doody?'},
{ 'id': 104, 'user_id': 12345, 'timestamp': 1509332453568, 'message': 'Howdy does?'},
{ 'id': 105, 'user_id': 23456, 'timestamp': 1509334563568, 'message': 'Howdy doesnt?'},
{ 'id': 106, 'user_id': 34567, 'timestamp': 1509334663568, 'message': 'Howdy did?'},
{ 'id': 107, 'user_id': 12345, 'timestamp': 1509336763568, 'message': 'Howdy didnt?'},
{ 'id': 108, 'user_id': 23456, 'timestamp': 1509336881787, 'message': 'Howdy donts?'}];

const mockChats2 = [{
"id":  "554621bd-e143-456b-8d05-d35b3ed92886" ,
"message":  "hi again" ,
"room_id":  "0x05E1bE98863e80bCdaCa1DBec8BC399540cA448C" ,
"timestamp": 1509414996100 ,
"type":  "message" ,
"user_id":  "0xec70a161c6283bcec515a0a0c0bf11328f908602"
}, {
"id":  "a6e67ee5-c195-4d1a-a860-ea3e3f7d3628" ,
"message":  "hi from FF1 to R1" ,
"room_id":  "0x05E1bE98863e80bCdaCa1DBec8BC399540cA448C" ,
"timestamp": 1509400881591 ,
"type":  "message" ,
"user_id":  "0x39fe6dd88a687133bba440874b691e6a91249235"
}, {
"id":  "0fe6ab2d-0724-41ef-bde3-6a7db6f92f77" ,
"message":  "hellop" ,
"room_id":  "0x05E1bE98863e80bCdaCa1DBec8BC399540cA448C" ,
"timestamp": 1509400910951 ,
"type":  "message" ,
"user_id":  "0x39fe6dd88a687133bba440874b691e6a91249235"
}, {
"id":  "3b848959-c488-405e-9ba6-4561afc37bdd" ,
"message":  "hi again" ,
"room_id":  "0x829f378fb8b8b5bc7946fea086383237b770d26c" ,
"timestamp": 1509401008581 ,
"type":  "message" ,
"user_id":  "0x39fe6dd88a687133bba440874b691e6a91249235"
}, {
"id":  "3797ed89-b146-4d17-816b-5c1b23e46cfe" ,
"message":  "hi from ff1 to R2" ,
"room_id":  "0x829f378fb8b8b5bc7946fea086383237b770d26c" ,
"timestamp": 1509400931542 ,
"type":  "message" ,
"user_id":  "0x39fe6dd88a687133bba440874b691e6a91249235"
}, {
"id":  "c2adf825-2dd6-4186-8acb-727605cb6734" ,
"message":  "me2" ,
"room_id":  "0x829f378fb8b8b5bc7946fea086383237b770d26c" ,
"timestamp": 1509401043325 ,
"type":  "message" ,
"user_id":  "0x39fe6dd88a687133bba440874b691e6a91249235"
}, {
"id":  "7a475dcc-cb0e-4841-b51d-146cb9099d7f" ,
"message":  "hi from c2 to R1" ,
"room_id":  "0x05E1bE98863e80bCdaCa1DBec8BC399540cA448C" ,
"timestamp": 1509400856781 ,
"type":  "message" ,
"user_id":  "0xec70a161c6283bcec515a0a0c0bf11328f908602"
}, {
"id":  "e8cd83db-2ed2-4299-b012-4c8c214f03a0" ,
"message":  "hi from ff2 to r1" ,
"room_id":  "0x05E1bE98863e80bCdaCa1DBec8BC399540cA448C" ,
"timestamp": 1509400906619 ,
"type":  "message" ,
"user_id":  "0x39fe6dd88a687133bba440874b691e6a91249235"
}, {
"id":  "4ab38f54-ff8b-4f5e-98d3-a1fab247e473" ,
"message":  "hi from ff2 to R2" ,
"room_id":  "0x829f378fb8b8b5bc7946fea086383237b770d26c" ,
"timestamp": 1509400953964 ,
"type":  "message" ,
"user_id":  "0x39fe6dd88a687133bba440874b691e6a91249235"
}, {
"id":  "4adfa85d-3c00-4513-a8a0-3aeb8383d777" ,
"message":  "still here" ,
"room_id":  "0x829f378fb8b8b5bc7946fea086383237b770d26c" ,
"timestamp": 1509401037580 ,
"type":  "message" ,
"user_id":  "0xec70a161c6283bcec515a0a0c0bf11328f908602"
}, {
"id":  "b046c7b4-4bf5-42f1-bb20-b0c5a4581b62" ,
"message":  "hi from c1 to R2" ,
"room_id":  "0x829f378fb8b8b5bc7946fea086383237b770d26c" ,
"timestamp": 1509400979025 ,
"type":  "message" ,
"user_id":  "0xec70a161c6283bcec515a0a0c0bf11328f908602"
}, {
"id":  "f89c25ad-7c20-4a5d-a9fe-aa553325bbb5" ,
"message":  "hi from c1 to R1" ,
"room_id":  "0x05E1bE98863e80bCdaCa1DBec8BC399540cA448C" ,
"timestamp": 1509400837989 ,
"type":  "message" ,
"user_id":  "0xec70a161c6283bcec515a0a0c0bf11328f908602"
}, {
"id":  "7143c245-8fc0-4e0f-88f6-d88ce6e05468" ,
"message":  "hi from c2 to R2" ,
"room_id":  "0x829f378fb8b8b5bc7946fea086383237b770d26c" ,
"timestamp": 1509400996108 ,
"type":  "message" ,
"user_id":  "0xec70a161c6283bcec515a0a0c0bf11328f908602"
}, {
"id":  "cdc2675e-8ea3-4e7e-8edd-d7c5a40d91be" ,
"message":  "hi again" ,
"room_id":  "0x829f378fb8b8b5bc7946fea086383237b770d26c" ,
"timestamp": 1509414996185 ,
"type":  "message" ,
"user_id":  "0xec70a161c6283bcec515a0a0c0bf11328f908602"
}];

export {initialMessages, mockChats1, mockChats2};
