import React from 'react';

class ChatInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    handleKeyPress(event){
        if (event.key === 'Enter' && !event.shiftKey) {
            event.preventDefault()
            this.props.parseMessage(event.target.value)
            event.target.value = ''
        }
    }

    render() {
        return (
            <form style={{width:'100%', height: '10%'}}>
                <textarea type="text"
                       style={{width: "inherit",
                               height: '100%',
                               borderColor: 'black',
                               borderWidth: '3px',
                               backgroundColor: "#F0F0F0",
                               padding: "15px",
                               resize: "none"
                               }}
                       placeholder="Type your text..."
                       onKeyPress={this.handleKeyPress}
                />
            </form>
        );
    }
}

export default ChatInput;

